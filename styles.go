package main

import "github.com/charmbracelet/lipgloss"

var (
	appStyle = lipgloss.NewStyle().Padding(1, 2)

	confirmTitleStyle = lipgloss.NewStyle().Bold(true)

	errorTextStyle      = lipgloss.NewStyle().Foreground(lipgloss.Color("#FF4136")).Bold(true)
	areYouSureTextStyle = lipgloss.NewStyle().Foreground(lipgloss.Color("#FF4136")).Bold(true)
	subtleTextStyle     = lipgloss.NewStyle()
)
