package main

import (
	"fmt"

	"github.com/charmbracelet/bubbles/viewport"
	tea "github.com/charmbracelet/bubbletea"
	"github.com/charmbracelet/lipgloss"
)

type confirmModel struct {
	ready    bool
	doRun    bool
	selected item
	viewport viewport.Model
	returnTo *selectingModel
}

func switchToConfirm(m *selectingModel) (confirmModel, tea.Cmd) {
	model := confirmModel{
		selected: m.list.SelectedItem().(item),
		returnTo: m,
	}
	return model, model.Init()
}

func (m confirmModel) View() string {
	if !m.ready {
		return "Initialising..."
	}

	return appStyle.Render(fmt.Sprintf("%s\n%s\n%s", m.headerView(), m.viewport.View(), m.footerView()))
}

func (m confirmModel) Init() tea.Cmd {
	return func() tea.Msg {
		// FIXME: Hacky way to get a WindowSizeMsg event sent
		frame_h, frame_v := appStyle.GetFrameSize()
		return tea.WindowSizeMsg{
			Height: m.returnTo.list.Height() + frame_v,
			Width:  m.returnTo.list.Width() + frame_h,
		}
	}
}

func (m confirmModel) headerView() string {
	return confirmTitleStyle.Render(m.selected.title)
}
func (m confirmModel) footerView() string {
	out := areYouSureTextStyle.Render("Are you sure?")
	out += subtleTextStyle.Render("  RET to continue, q to go back, ctrl+c to quit")
	return out
}

func (m confirmModel) Update(msg tea.Msg) (tea.Model, tea.Cmd) {
	var cmds []tea.Cmd

	switch msg := msg.(type) {
	case tea.KeyMsg:
		switch msg.String() {
		case "enter":
			m.doRun = true
			return m, tea.Quit
		case "ctrl+c":
			return m, tea.Quit
		case "q":
			return m.returnTo, nil
		}
	case tea.WindowSizeMsg:
		padding_h, padding_v := appStyle.GetFrameSize()
		headerHeight := lipgloss.Height(m.headerView())
		footerHeight := lipgloss.Height(m.footerView())
		verticalMarginHeight := headerHeight + footerHeight + padding_v

		if !m.ready {
			m.viewport = viewport.New(msg.Width-padding_h, msg.Height-verticalMarginHeight)
			m.viewport.YPosition = headerHeight + padding_v + 1
			m.viewport.SetContent(m.selected.contents)
			m.ready = true
		} else {
			m.viewport.Width = msg.Width - padding_h
			m.viewport.Height = msg.Height - verticalMarginHeight - padding_v
		}
	case fatalErrorMsg:
		fmt.Printf("encountered fatal error: %s\n", msg.Error())
		return m, tea.Quit
	}

	// Handle keyboard and mouse events in the viewport
	var cmd tea.Cmd
	m.viewport, cmd = m.viewport.Update(msg)
	cmds = append(cmds, cmd)
	return m, tea.Batch(cmds...)
}
