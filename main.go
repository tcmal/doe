package main

import (
	"fmt"
	"os"
	"os/exec"
	"strings"
	"syscall"

	tea "github.com/charmbracelet/bubbletea"
)

type item struct {
	title    string
	contents string
}

type fatalErrorMsg error

func (i item) Title() string       { return i.title }
func (i item) Description() string { return i.contents }
func (i item) FilterValue() string { return i.title }

func main() {
	if e := runProgram(); e != nil {
		fmt.Println(e)
		os.Exit(1)
	}
}

func runProgram() error {
	err := setupDB()
	if err != nil {
		return fmt.Errorf("Error connecting to KV store: %s", err)
	}

	initialFilter := ""
	if len(os.Args) > 1 {
		initialFilter = strings.Join(os.Args[1:], " ")
	}

	model, err := tea.NewProgram(StartSelecting(initialFilter), tea.WithMouseCellMotion()).Run()
	if err != nil {
		return fmt.Errorf("Error running program: %s", err)
	}
	if confirm, ok := model.(confirmModel); ok && confirm.doRun {
		sh, err := exec.LookPath("sh")
		if err != nil {
			return fmt.Errorf("could not find sh: %s", err)
		}

		args := []string{"-v", "-c"}
		args = append(args, "set -v; "+confirm.selected.contents)

		err = syscall.Exec(sh, args, os.Environ())
		if err != nil {
			return fmt.Errorf("could not exec: %s", err)
		}
	}

	return nil
}
