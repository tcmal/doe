# doe

[![No Maintenance Intended](http://unmaintained.tech/badge.svg)](http://unmaintained.tech/)

Doe is a simple TUI command snippet runner. You can add scripts to it, they'll be stored in `~/.doe`, and then you can pick and execute them later.
Use + to add snippets, d to delete them, and vim keybindings to select them. You can also run doe with `doe <search term>` to filter your initial results.

This was mostly an excuse for me to use [Bubble Tea](https://github.com/charmbracelet/bubbletea), which is a cool framework you should check out.
